/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio1;

import junit.framework.TestCase;
import org.junit.*;
/**
 *
 * @author brunoyfs
 */
public class calcularFatorialTeste {
    @Test
    public void teste() {
        Fatorial obj = new Fatorial();
        assertEquals(6, obj.calcularFatorial(3));
        assertEquals(1, obj.calcularFatorial(0));
        assertEquals(120, obj.calcularFatorial(5));
        assertEquals(3628800, obj.calcularFatorial(10));
    }
}
