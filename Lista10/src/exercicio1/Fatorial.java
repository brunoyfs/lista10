public class Fatorial {
    
    public static void calcularFatorial(double numero) {
        double fatorial = numero;
        
        for (double i = numero; i > 1; i--) {
            fatorial = fatorial * (i - 1);
        }
        
        System.out.println(fatorial);
    }

     public static void main(String []args){
        calcularFatorial(10);
     }
}
