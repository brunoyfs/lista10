/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio6;

import java.text.*;
import java.util.*;

public abstract class Cartao {
    // constants da classe
    public static final int VISA = 1;
    public static final int MASTERCARD = 2;
    public static final int AMEX = 3;
    public static final int DINERS = 4;
 
    private String numero;
    private int mes;
    private int ano;
    private int bandeira;

    public Cartao(int bandeira, String numero, int mes, int ano) {
        this.bandeira = bandeira;
        this.numero = numero;
        this.mes = mes;
        this.ano = ano;
    }
    
    public int getMes() {
        return mes;
    }
    
    public int getAno() {
        return ano;
    }
    
    public String getNumero() {
        return numero;
    }
    
    public boolean validar(int bandeira, String numero, String validade) {
	boolean validadeOK = false;
        
	validadeOK = isValidadeOK(validade);
        
	if (!validadeOK) {
            return false;
	}
	else {
	// ----- PREFIXO E TAMANHO -----
            String formatado = numero.replaceAll("\\D", "");
            boolean formatoOK = false;
            formatoOK = isFormatoOK(bandeira, formatado);
            
            if (!formatoOK) {
                return false;
            }
            else {
                return isNumeroOK(formatado);
            }
	}
    }

    private boolean isValidadeOK(String validade) {
	boolean validadeOK;
        
	// ----- VALIDAE ------
	Date dataValidade = null;
        
	try {
            dataValidade = new SimpleDateFormat("MM/YYYY").parse(validade);
	} catch (ParseException e) {
            return false;
	}
        
	Calendar calValidade = new GregorianCalendar();
	calValidade.setTime(dataValidade);
	// apenas mês e ano são utilizados na validação
	Calendar calTemp = new GregorianCalendar();
	Calendar calHoje = (GregorianCalendar) calValidade.clone();
	calHoje.set(Calendar.MONTH, calTemp.get(Calendar.MONTH));
	calHoje.set(Calendar.YEAR, calTemp.get(Calendar.YEAR));
	validadeOK = calHoje.before(calValidade);
        
	return validadeOK;
    }
    
    private boolean isFormatoOK(int bandeira, String formatado) {
	boolean formatoOK;
        
	switch (bandeira) {
            case VISA: // tamanhos 13 ou 16, prefixo 4.
                formatoOK = formatado.matches("^4(\\d{12}|\\d{15})$");
                break;

            case MASTERCARD: // tamanho 16, prefixos 55
                formatoOK = formatado.matches("^5[1-5]\\d{14}$");
                break;

            case AMEX: // tamanho 15, prefixos 34 e 37
                formatoOK = formatado.matches("^3[47]\\d{13}$");
                break;

            case DINERS: // tamanho 14, prefixos 305, 36 e 38
                formatoOK = formatado.matches("^3[68]\\d{12}|0[0-5]\\d{11})$");
                break;

            default:
                formatoOK = false;
                break;
	}
        
	return formatoOK;
    }
    
    private boolean isNumeroOK(String formatado) {
	// -----NÚMERO-----
	// fórmula de LUHN (http://www.merriampark.com/anatomycc.htm)
        
	int soma = 0;
	int digito = 0;
	int somafim = 0;
	boolean multiplica = false;
        
        for (int i = formatado.length()-1; i >= 0; i--) {
            digito = Integer.parseInt(formatado.substring(i, i+1));
            
            if (multiplica) {
                somafim = digito * 2;
                if (somafim > 9) {
                    somafim -= 9;
                }
            } else {
                somafim = digito;
            }
            
            soma += somafim;
            multiplica = !multiplica;
	}
        
	int resto = soma % 10;
        
	if (resto == 0) {
            return true;
	} else {
            return false;
	}
    }
}