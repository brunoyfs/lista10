/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio6;

import exercicio6.Cartao;

public class FormatoIncorretoException extends RuntimeException {
    public FormatoIncorretoException (Cartao cartao) {
        super(cartao.getNumero());
    }
}
