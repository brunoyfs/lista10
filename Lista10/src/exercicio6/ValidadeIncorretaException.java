/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio6;

import exercicio6.Cartao;

public class ValidadeIncorretaException extends RuntimeException {
    public ValidadeIncorretaException (Cartao cartao) {
        super(cartao.getMes() + "/" + cartao.getAno());
    }
}